
---
layout: "post"
title: "Contributors wanted"
date: 2018-02-05
tags:
    - fediverse
preview: "Jason Robinson calls out for help to make The-Federation.info a stats hub for all projects. Help adding OStatus and ActivityPub networks to the list."
url: "https://socialhome.network/content/1092839/organization-news-the-federation-info-is-now"
lang: en
image: ""
wanted: true
---


Jason Robinson, Socialhome developer, calls out for help to make The-Federation.info into a stats hub for all projects. Help adding OStatus and ActivityPub networks to the list.
More details [here](https://socialhome.network/content/1092839/organization-news-the-federation-info-is-now).



---
layout: "post"
title: "Why use federated networks (WIP)"
date: 2018-02-27
tags:
    - fediverse
preview:
  "This article is a stub. You can help the Fediverse wiki by expanding it. Anyone welcome to contribute. It may be a less technical article, for your friend and your grandma. Explaining with vivid examples why privacy is important, how corporate entities are misusing their capabilities, etc."
url: "/en/post/Why-use-federated-networks"
lang: en
banner: "banner.jpg"
featured: true
---

This article is a stub. You can help the Fediverse wiki by expanding it. Anyone welcome to contribute.

It may be a less technical article, for your friend and your grandma. Explaining with vivid examples why privacy is important, how corporate entities are misusing their capabilities, etc.

See [this article](https://signal.org/blog/the-ecosystem-is-moving) by M. Marlinspike elaborating on why federation *is not* the future.

There are few articles about the strong points of federated / decentralized networks on the net. Some of those are research papers using obscure scientific language. Would be nice to have a thorough writing on why Moxy didn't get the point of federation. May be its point is not about providing same smooth experience for everyone (which, really, is quite hard). May be its point is in promoting a new concept, users expecting to lose certain convenient features of centralized systems in exchange for benefits that a centralized system can not guarantee. Singling out these benefits, as well as describing weak points of centralized systems is desirable. Add facts / links to many recent data breaches, misuse of centralized networks, etc...

I'll add some links to research papers on decentralization later. Please, don't hesitate to join and help writing this article.

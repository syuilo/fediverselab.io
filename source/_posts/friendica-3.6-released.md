
---
layout: "post"
title: "Friendica 3.6 released"
date: 2018-03-24
tags:
    - friendica
preview: "Friendica the Tazmans Flax-​lily is the name of the release, a tribute to Tony Baldwin. The big adventure of reorganising code brings many updates."
url: "https://friendi.ca/2018/03/23/friendica-3-6-the-tazmans-flax-lily-released"
lang: en
image: ""
---

Friendica the Tazmans Flax-​lily is the name of the release, a tribute to the memory of Tony “tazman” Baldwin. The big adventure of reorganising the code brings many updates and bug fixes.
More info [here](https://friendi.ca/2018/03/23/friendica-3-6-the-tazmans-flax-lily-released).

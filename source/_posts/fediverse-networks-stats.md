
---
layout: "post"
title: "Fediverse statistics"
date: 2018-04-27
tags:
    - fediverse
preview: "Have a look at 'fediverse.network' - new stats. Glue them together with 'the-federation.info' and you'll get a good picture of Fediverse."
url: "https://fediverse.network"
lang: en
image: ""
---

Have a look at [fediverse.network](https://fediverse.network) - new stats. As any numbers in the federated environment, these numbers are not precise, but still valuable. Glue them together with [the-federation.info](https://the-federation.info) and you'll get a good picture of the Fediverse.

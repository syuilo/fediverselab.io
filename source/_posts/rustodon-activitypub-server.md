
---
layout: "post"
title: "+ 1 Rust project"
date: 2018-04-11
tags:
    - fediverse
preview: "Here comes Rustodon - Mastodon-compatible, ActivityPub speaking server in Rust. Currently under development. Go check it out, join and contribute."
url: "https://github.com/rustodon/rustodon"
lang: en
image: ""
---

As Fediverse is growing, we shall likely see more and more projects appearing, which is brilliant!
And here comes [Rustodon](https://github.com/rustodon/rustodon) - a Mastodon-compatible, ActivityPub-speaking server in Rust. It's currently heavy under development but you can already check out the progress or join and contribute.

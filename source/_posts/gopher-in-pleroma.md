
---
layout: "post"
title: "Gopher Support in Pleroma"
date: 2018-04-02
tags:
    - pleroma
preview: "Pleroma is a high-performance and low-resource server meant to run even on small devices like Raspberry Pi. Now it supports Gopher!"
url: "https://blog.soykaf.com/post/gopher-support-in-pleroma"
lang: en
image: ""
---

As you may know, Pleroma is a high-performance and low-resource server meant to run even on small devices like Raspberry Pi. And now it supports Gopher protocol. Simplicity is perfection.
Read the announcement [here](https://blog.soykaf.com/post/gopher-support-in-pleroma).

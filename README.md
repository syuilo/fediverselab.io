
## Fediverse WIKI

A quick look into Fediverse networks

~~~
Work in progress
~~~

### Important

- Logo icon on Pleroma page is [unofficial](https://gitlab.com/lostinlight/per_aspera_ad_astra/tree/master/pleroma-concept), with @lain's permission used until the official logo is chosen
- "World" text on postActiv page is by @Maiyannah Bishop, main postActiv developer

### Goals

1. *Road Making*: Provide links to information scattered around the internet. Make it easier for newcomers to get acquainted with the idea in general and with every federated network in particular.

2. *Team Building*: Unite all federated social networks under 'Fediverse' name. Highlight the sense and spirit of a Fediverse community.

### Contributing

> All the files you need are in `/source/_data` and `/source/_posts` folders

Help welcome with the following pages:
  - 'Knowledge' page: two stub articles "FAQ for newcomers" and "Why use federated networks" waiting to be written.

### Structure

#### Fediverse page

*Data located in*: `/source/_data/fediversei18n.json` file.

#### Chronicles page

> Latest news about major releases, development, interviews, articles, related stuff.

*Data located in*: `/source/_posts` folder

All posts are available via RSS subscription.
'Preview' is rendered on Chronicles page (limited ammount), text after metadata is shown in RSS (unlimited).

Every post must have the following __metadata__:

```
layout: "post"
title: "some title"
date: 2222-01-25
tags:
    - pleroma
preview:
  "short gist..."
url: "https://test.com"
lang: en
```

`Tags`: networkName / fediverse; most posts will have 1 tag
`Preview`: limit 150 characters - for "regular" and "wanted" posts, limit 350 characters - for "featured" post.

__Optional metadata__:

```
wanted: true
featured: true
banner: "pic.jpg"
```

`Wanted` and `featured` can't be mixed and are temporary, should be removed from older post when creating a new "featured" or "wanted" post.
`banner`: required for "featured" posts only.

#### Knowledge page

*Data located in*: `/source/_data/knowledge.json` file.

#### Portal page

Supposed to link to a page helping to choose any pod on any network (just-an-idea for now)

### Theme File Structure

- Develop styles in  `/themes/starter/assets/scss`
- Develop scripts in `/themes/starter/assets/scripts`

### Setup

1. Run `npm install` in main project's folder
2. Run `gulp` in  `/themes/starter` folder
3. In second terminal, run `hexo server` in main folder, to start the server and preview at localhost


### Translating

Only English version.
Adding another language requires additions to `_config` file and `/themes/starter/languages` folder.

Mandatory for translating:
- create lang folder (`/source/*lang*`)
- common strings (`/themes/starter/languages/en`)
- `/source/_data` folder: fediversei18n.json, worldi18n.json

Translating Chronicles or Knowledge pages content doesn't make sense. They will be mostly a collection of external articles.
